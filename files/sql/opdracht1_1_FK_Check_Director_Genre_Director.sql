-----------------------
-- Negative selection query
-----------------------
SELECT DG.*
FROM dbo.Imported_Director_Genre AS DG
       LEFT JOIN
     dbo.Imported_Directors AS D
     ON DG.Did = D.Id
WHERE D.Id IS NULL
-- Time 0:00 3702 Records

-----------------------
-- Delete
-----------------------
DELETE DG
FROM dbo.Imported_Director_Genre AS DG
         LEFT JOIN
     dbo.Imported_Directors AS D
     ON DG.Did = D.Id
WHERE D.Id IS NULL
-- 3702 rows affected in 334 ms