USE MASTER;
GO

-----------------------
-- Drop database if the database exists
-----------------------
IF DB_ID('odisee') IS NOT NULL
  DROP DATABASE odisee;
GO

-----------------------
-- Create database
-----------------------
CREATE DATABASE odisee;
GO

USE odisee;
GO


-----------------------
-- DDL
-----------------------
CREATE TABLE Person
(
  person_id int         NOT NULL,
  lastname  varchar(50) NOT NULL,
  firstname varchar(50) NOT NULL,
  gender    char(1)     NULL,
  PRIMARY KEY (person_id),
  CHECK (gender in ('M', 'F')),
  CONSTRAINT AK_Person UNIQUE (firstname, lastname)
);
GO

CREATE TABLE Movie
(
  movie_id         int           NOT NULL,
  title            varchar(255)  NOT NULL,
  duration         int           NULL,
  description      varchar(255)  NULL,
  publication_year int           NULL,
  cover_image      varchar(255)  NULL,
  previous_part    int           NULL,
  price            numeric(5, 2) NOT NULL,
  URL              varchar(255)  NULL,
  PRIMARY KEY (movie_id),
  FOREIGN KEY (previous_part) REFERENCES Movie (movie_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
GO

CREATE TABLE Genre
(
  genre_name  varchar(255) NOT NULL,
  description varchar(255) NULL,
  PRIMARY KEY (genre_name)
);
GO

CREATE TABLE Movie_Cast
(
  movie_id  int          NOT NULL,
  person_id int          NOT NULL,
  role      varchar(255) NOT NULL,
  PRIMARY KEY (movie_id, person_id, role),
  FOREIGN KEY (movie_id) REFERENCES Movie (movie_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (person_id) REFERENCES Person (person_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
);
GO

CREATE TABLE Movie_Directors
(
  movie_id  int NOT NULL,
  person_id int NOT NULL,
  PRIMARY KEY (movie_id, person_id),
  FOREIGN KEY (movie_id) REFERENCES Movie (movie_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (person_id) REFERENCES Person (person_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
);
GO

CREATE TABLE Movie_Genre
(
  movie_id   int          NOT NULL,
  genre_name varchar(255) NOT NULL,
  PRIMARY KEY (movie_id, genre_name),
  FOREIGN KEY (movie_id) REFERENCES Movie (movie_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (genre_name) REFERENCES Genre (genre_name)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
);
GO