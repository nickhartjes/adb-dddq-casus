-----------------------
-- Negative selection query
-----------------------
SELECT DG.Did, DG.Genre
FROM dbo.Imported_Director_Genre AS DG
GROUP BY DG.Did, DG.Genre
HAVING COUNT(*) > 1;
-- 0 rows retrieved in 1 s 70 ms (execution: 1 s 37 ms, fetching: 33 ms)

-----------------------
-- Remove duplicates except one
-----------------------
WITH duplicates AS (
    SELECT Did, Genre, ROW_NUMBER() OVER (PARTITION BY Did, Genre ORDER BY Did, Genre) RowNumber
    FROM dbo.Imported_Director_Genre
)
DELETE FROM duplicates
WHERE RowNumber > 1
GO
--  completed in 1 s 377 ms
