-- Klanten van ODISEE kunnen reviews geven van films.
-- De klant schrijft een recensie en geeft cijfers voor de rubrieken Cinematography, Plot, Acting en Music and Sound.
-- Voor een geldige recensie zijn cijfers voor Plot en Acting verplicht en dient minimaal een van de rubrieken
-- Cinematography en Music and Sound te worden beoordeeld.
-- Als hieraan is voldaan, dan wordt de recensie geaccepteerd en wordt het totaalcijfer (een 7.8 in het onderstaande voorbeeld) berekend.
--
-- alter table odisee1.dbo.Review
--     drop constraint CK_category_filled_check
-- go

DROP FUNCTION IF EXISTS check_ReviewCategory;
GO

CREATE FUNCTION check_ReviewCategory(@category VARCHAR(255), @product_id INT, @email_address VARCHAR(255))
    RETURNS BIT
AS
BEGIN
    DECLARE
        @Answer BIT;
    SET @Answer = CASE
                      WHEN EXISTS(
                              SELECT *
                              FROM odisee1.dbo.Review_Category AS RC
                              WHERE RC.product_id = @product_id
                                AND RC.email_address = @email_address
                                AND RC.category_name = @category
                          )
                          THEN 1
                      ELSE 0 END;
    RETURN @Answer;
END
GO


ALTER TABLE odisee1.dbo.Review
    ADD CONSTRAINT CK_category_filled_check
        CHECK (
                dbo.check_ReviewCategory('Plot', product_id, email_address) = 1
                AND
                dbo.check_ReviewCategory('Acting', product_id, email_address) = 1
                AND
                (
                            dbo.check_ReviewCategory('Cinematography', product_id, email_address) = 1
                        OR
                            dbo.check_ReviewCategory('Music and Sound', product_id, email_address) = 1
                    )
            );
GO


-----------------------
-- Test 01
-----------------------
INSERT INTO odisee1.dbo.Review (product_id, email_address, review_date, description, average_score)
VALUES (412539, 'nickhartjes@gmail.com', GETDATE(), 'Text', (
    SELECT AVG(score)
    FROM odisee1.dbo.Review_Category
    GROUP BY product_id, email_address
    HAVING product_id = 412539
       AND email_address = 'nickhartjes@gmail.com'
) as average_score);
-- Fail: [23000][547] The INSERT statement conflicted with the CHECK constraint "CK_category_filled_check". The conflict occurred in database "odisee1", table "dbo.Review".

-----------------------
-- Test 02
-----------------------
INSERT INTO odisee1.dbo.Review_Category (product_id, email_address, category_name, score)
VALUES (412539, 'nickhartjes@gmail.com', 'Plot', 8),
       (412539, 'nickhartjes@gmail.com', 'Acting', 8);
GO

INSERT INTO odisee1.dbo.Review (product_id, email_address, review_date, description, average_score)
VALUES (412539, 'nickhartjes@gmail.com', GETDATE(), 'Text', (
    SELECT AVG(score)
    FROM odisee1.dbo.Review_Category
    GROUP BY product_id, email_address
    HAVING product_id = 412539
       AND email_address = 'nickhartjes@gmail.com'
) as average_score);
-- Fail: [23000][547] The INSERT statement conflicted with the CHECK constraint "CK_category_filled_check". The conflict occurred in database "odisee1", table "dbo.Review".
GO

DELETE
FROM odisee1.dbo.Review_Category
WHERE product_id = 412539
  AND email_address = 'nickhartjes@gmail.com';
GO


-----------------------
-- Test 03
-----------------------
INSERT INTO odisee1.dbo.Review_Category (product_id, email_address, category_name, score)
VALUES (412539, 'nickhartjes@gmail.com', 'Plot', 8),
       (412539, 'nickhartjes@gmail.com', 'Acting', 8),
       (412539, 'nickhartjes@gmail.com', 'Cinematography', 8);


INSERT INTO odisee1.dbo.Review (product_id, email_address, review_date, description, average_score)
VALUES (412539, 'nickhartjes@gmail.com', GETDATE(), 'Text', (
    SELECT AVG(score)
    FROM odisee1.dbo.Review_Category
    GROUP BY product_id, email_address
    HAVING product_id = 412539
       AND email_address = 'nickhartjes@gmail.com'
) as average_score);
-- 1 row affected in 18 ms

DELETE
FROM odisee1.dbo.Review_Category
WHERE product_id = 412539
  AND email_address = 'nickhartjes@gmail.com';