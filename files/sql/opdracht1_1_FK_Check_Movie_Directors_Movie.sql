-----------------------
-- Negative selection query
-----------------------
SELECT MD.*
FROM dbo.Imported_Movie_Directors AS MD
WHERE MD.Mid NOT IN (
  SELECT M.Id
  FROM dbo.Imported_Movie AS M
);
-- Time 1:48 0 Records

SELECT MD.*
FROM dbo.Imported_Movie_Directors AS MD
       LEFT JOIN
     dbo.Imported_Movie AS M
     ON MD.Mid = M.Id
WHERE M.Id IS NULL
-- Time 0:00 0 Records

-----------------------
-- Delete
-----------------------
DELETE MD
FROM dbo.Imported_Movie_Directors AS MD
         LEFT JOIN
     dbo.Imported_Movie AS M
     ON MD.Mid = M.Id
WHERE M.Id IS NULL
-- 0 rows affected in 3 s 994 ms

-----------------------
-- Test set
-----------------------
DECLARE @mid varchar(10);
DECLARE @did varchar(10);
SET @mid = 99999999;
SET @did = 1;

-- Check base
SELECT COUNT(*)
FROM dbo.Imported_Movie_Directors AS MD
WHERE MD.Did = @did
  AND MD.Mid = @mid;
-- Result = 0

SELECT COUNT(*)
FROM dbo.Imported_Movie AS M
WHERE M.Id = @mid;
-- Result = 0

-- Insert test set
INSERT INTO dbo.Imported_Movie_Directors (Did, Mid)
VALUES (@did, @mid);

-- Check inserted data
SELECT COUNT(*)
FROM dbo.Imported_Movie_Directors AS MD
WHERE MD.Did = @did
  AND MD.Mid = @mid;
-- Result = 1

-- Now run the negative selection query
-- the count needs to be 1

-- Remove test data
DELETE
FROM dbo.Imported_Movie_Directors
WHERE Mid = @mid
  AND Did = @did;