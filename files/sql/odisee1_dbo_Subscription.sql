INSERT INTO odisee1.dbo.Subscription (country_name, subscription_type, monthly_fee, discount_percentage) VALUES ('The Netherlands', 'Basic', 4.00, 0.00)
, ('The Netherlands', 'Premium', 5.00, 20.00)
, ('The Netherlands', 'Pro', 6.00, 40.00)
, ('Germany', 'Basic', 3.50, 0.00)
, ('Germany', 'Premium', 5.00, 20.00)
, ('Germany', 'Pro', 6.50, 40.00)
, ('United Kingdom', 'Basic', 3.50, 0.00)
, ('United Kingdom', 'Premium', 4.50, 20.00)
, ('United Kingdom', 'Pro', 5.50, 40.00);