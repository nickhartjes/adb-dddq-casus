-----------------------
-- Negative selection query
-----------------------
SELECT C.*
FROM dbo.Imported_Cast AS C
WHERE C.Pid NOT IN (
  SELECT P.Id
  FROM dbo.Imported_Person AS P
);
-- Time: na 7 minuten uitgezet

SELECT C.*
FROM dbo.Imported_Cast AS C
       LEFT JOIN
     dbo.Imported_Person AS P
     ON C.Mid = P.Id
WHERE P.Id IS NULL
-- Time 0:06 98940 Records

-----------------------
-- Delete
-----------------------
DELETE C
FROM dbo.Imported_Cast AS C
         LEFT JOIN
     dbo.Imported_Person AS P
     ON C.Mid = P.Id
WHERE P.Id IS NULL
-- 98919 rows affected in 13 s 579 ms