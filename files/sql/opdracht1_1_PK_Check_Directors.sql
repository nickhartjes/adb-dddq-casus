-----------------------
-- Negative selection query
-----------------------
SELECT MD.Id
FROM dbo.Imported_Directors AS MD
GROUP BY MD.Id
HAVING COUNT(*) > 1;
-- 0 rows retrieved in 192 ms (execution: 140 ms, fetching: 52 ms)

-----------------------
-- Remove duplicates except one
-----------------------
WITH duplicates AS
         (
             SELECT Id, ROW_NUMBER() OVER (PARTITION BY Id ORDER BY Id) RowNumber
             FROM dbo.Imported_Directors
         )
DELETE
FROM duplicates
WHERE RowNumber > 1
-- completed in 196 ms
GO

-----------------------
-- Test set negative selection query
-----------------------
DECLARE @id varchar(10);
SET @id = 999999;

-- Check base
SELECT COUNT(*) AS Base
FROM dbo.Imported_Directors AS MD
WHERE MD.Id = @id;
-- Result = 0

-- Insert test set
INSERT INTO dbo.Imported_Directors(Id, FName, LName)
VALUES (@id, 'test', 'test'),
       (@id, 'test', 'test');

-- Check inserted data
SELECT COUNT(*) AS Base
FROM dbo.Imported_Directors AS MD
WHERE MD.Id = @id;
-- Result = 2

-- Now run the negative selection query

-- Remove test data
DELETE
FROM dbo.Imported_Directors
WHERE Id = @id;