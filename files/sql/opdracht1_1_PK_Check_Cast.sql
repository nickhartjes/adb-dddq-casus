-----------------------
-- Negative selection query
-----------------------
SELECT C.Mid, C.Pid
FROM dbo.Imported_Cast AS C
GROUP BY C.Mid, C.Pid
HAVING COUNT(*) > 1;
-- Time 0:04 719 Records

-----------------------
-- Remove duplicates except one
-----------------------
WITH duplicates AS
         (
             SELECT Mid, Pid, ROW_NUMBER() OVER (PARTITION BY Mid, Pid ORDER BY Mid, Pid) RowNumber
             FROM dbo.Imported_Cast
         )
DELETE
FROM duplicates
WHERE RowNumber > 1
GO
-- 1141 rows affected in 11 s 442 ms