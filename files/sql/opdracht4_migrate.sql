USE Master
GO

-----------------------
-- Person
-----------------------
INSERT INTO odisee1.dbo.Person (person_id, last_name, first_name, gender)
SELECT CAST(P.person_id as int)         AS Id,
       CAST(P.firstname AS varchar(50)) AS first_name,
       CAST(P.lastname AS varchar(50))  AS last_name,
       CAST(P.gender AS varchar(1))     AS gender
FROM odisee.dbo.Person AS P;
-- (879492 rows affected)
GO

-----------------------
-- ProductType
-----------------------
INSERT INTO odisee1.dbo.Product_Type
VALUES ('Movie'),
       ('Game');
-- (2 rows affected)
GO

-----------------------
-- Movie --> Product
-----------------------
INSERT INTO odisee1.dbo.Product(product_id, title, duration, description, publication_year, cover_image,
                                previous_product_id, movie_default_price, url, product_type)
SELECT CAST(M.movie_id as int)             AS product_id,
       CAST(M.title AS varchar(255))       AS title,
       CAST(M.duration AS int)             AS duration,
       CAST(M.description AS varchar(255)) AS description,
       CAST(M.publication_year AS int)     AS publication_year,
       CAST(M.cover_image AS varchar(255)) AS cover_image,
       CAST(M.previous_part AS int)        AS previous_part,
       CAST(M.price AS numeric(5, 2))      AS price,
       CAST(M.URL AS varchar(255))         AS url,
       'Movie'
FROM odisee.dbo.Movie AS M;
-- (388264 rows affected)
GO

UPDATE odisee1.dbo.Product 
SET movie_default_price = 2
WHERE movie_default_price IS NULL OR movie_default_price = 0;
-- (388264 rows affected)
GO

-----------------------
-- Genre
-----------------------
INSERT INTO odisee1.dbo.Genre(genre_name)
SELECT CAST(G.genre_name AS varchar(255)) AS genre_name
FROM odisee.dbo.Genre AS G;
-- (21 rows affected)
GO

-----------------------
-- Movie_Genre
-----------------------
INSERT INTO odisee1.dbo.Product_Genre(product_id, genre_name)
SELECT CAST(MG.movie_id AS int)            AS product_id,
       CAST(MG.genre_name AS varchar(255)) AS genre_name
FROM odisee.dbo.Movie_Genre AS MG
-- (395114 rows affected)
GO

-----------------------
-- Movie_Directors
-----------------------
INSERT INTO odisee1.dbo.Movie_Director(product_id, person_id)
SELECT CAST(MD.movie_id AS int)  AS product_id,
       CAST(MD.person_id AS int) AS person_id
FROM odisee.dbo.Movie_Directors AS MD
-- (198023 rows affected)
GO

-----------------------
-- Movie_Cast
-----------------------
INSERT INTO odisee1.dbo.[Cast](product_id, person_id, role)
SELECT CAST(MC.movie_id AS int)      AS product_id,
       CAST(MC.person_id AS int)     AS person_id,
       CAST(MC.role AS varchar(255)) AS [role]
FROM odisee.dbo.Movie_Cast AS MC
GO

-----------------------
-- Country
-----------------------
INSERT INTO odisee1.dbo.Country_Currency
VALUES ('Euro'),
       ('Pound'),
       ('Lire');
GO

-----------------------
-- Country
-----------------------
INSERT INTO odisee1.dbo.Country
VALUES ('The Netherlands', 'Euro'),
       ('Germany', 'Euro'),
       ('United Kingdom', 'Pound');


-----------------------
-- Game --> Product
-- Before you can run this, you need to run gamelist.sql added in this document
-----------------------
DECLARE
    @id int;
SELECT @id = max(product_id) + 1
from odisee1.dbo.Product;
INSERT INTO odisee1.dbo.Product(product_id, title, description, publication_year, cover_image,
                                movie_default_price, product_type)
SELECT ROW_NUMBER() over (order by M.id) + @id AS product_id,
       CAST(M.Titel AS varchar(255))           AS title,
       CAST(M.HtmlDetailTekst AS varchar(255)) AS description,
       CASE
           WHEN TRY_CAST(RIGHT(CAST(M.ReleaseDatum AS varchar(10)), 4) AS int) IS NULL
               THEN 2017
           ELSE TRY_CAST(RIGHT(CAST(M.ReleaseDatum AS varchar(10)), 4) AS int)
           END                                 AS publication_year,
       CAST(M.LocalBgImgUrl AS varchar(255))   AS cover_image,
       0.00                                    AS price,
       'Game'                                  As product_type
FROM odisee1.dbo.gameslist AS M
WHERE M.Titel IS NOT NULL
  AND CAST(M.Platform AS varchar(255))  = 'pc';
GO

-----------------------
-- Game --> Product
-----------------------
INSERT INTO odisee1.dbo.Default_Game_Price(country_name, product_id, default_price)
SELECT 'The Netherlands' AS country_name,
       P.product_id,
       2.00              AS default_price
FROM odisee1.dbo.Product AS P
WHERE P.product_type = 'Game'
GO

INSERT INTO odisee1.dbo.Default_Game_Price(country_name, product_id, default_price)
SELECT 'Germany' AS country_name,
       P.product_id,
       2.25      AS default_price
FROM odisee1.dbo.Product AS P
WHERE P.product_type = 'Game'
GO

INSERT INTO odisee1.dbo.Default_Game_Price(country_name, product_id, default_price)
SELECT 'United Kingdom' AS country_name,
       P.product_id,
       1.75             AS default_price
FROM odisee1.dbo.Product AS P
WHERE P.product_type = 'Game'
GO

-----------------------
-- Users
-----------------------
INSERT INTO odisee1.dbo.[User] (email_address, country_name, username, first_name, last_name, gender)
VALUES ('nickhartjes@gmail.com', 'The Netherlands', 'nickhartjes', 'Nick', 'Hartjes', 'M'),
       ('joey.stoffels@gmail.com', 'The Netherlands', 'joey1337noScope', 'Joey', 'Stoffels', 'M'),
       ('hank.schrader@dea.gov', 'United Kingdom', 'specialagent', 'Henk', 'Schrader', 'M'),
       ('vel.mauris@uteratSed.org', 'Germany', 'markwinters', 'Mark', 'Winters', 'M'),
       ('sociis@idrisusquis.net', 'Germany', 'zacherywooten', 'Zachery', 'Wooten', 'M'),
       ('sit@Integer.net', 'The Netherlands', 'russellpage', 'Russell', 'Page', 'M'),
       ('ac@erat.co.uk', 'The Netherlands', 'lionelarnold', 'Lionel', 'Arnold', 'M'),
       ('et@Etiamligula.edu', 'Germany', 'roganchaney', 'Rogan', 'Chaney', 'M'),
       ('hendrerit@tellus.ca', 'Germany', 'merrittsaunders', 'Merritt', 'Saunders', 'M'),
       ('molestie@turpisegestasAliquam.edu', 'United Kingdom', 'eaganrodgers', 'Eagan', 'Rodgers', 'M'),
       ('lobortis.augue.scelerisque@sempercursusInteger.co.uk', 'Germany', 'granthammond', 'Grant', 'Hammond', 'M'),
       ('ut.mi@aliquetodio.org', 'The Netherlands', 'lylekidd', 'Lyle', 'Kidd', 'M'),
       ('faucibus@enim.org', 'Germany', 'ulricbentley', 'Ulric', 'Bentley', 'M'),
       ('ornare.Fusce.mollis@rutrumjustoPraesent.edu', 'United Kingdom', 'wesleyrowe', 'Wesley', 'Rowe', 'M'),
       ('magna.a.tortor@leoMorbi.com', 'Germany', 'alfonsosmall', 'Alfonso', 'Small', 'M'),
       ('risus.Donec.nibh@euismodac.com', 'United Kingdom', 'christianmason', 'Christian', 'Mason', 'M'),
       ('Cras.eu@Cras.net', 'The Netherlands', 'timonfigueroa', 'Timon', 'Figueroa', 'M'),
       ('Nam.tempor@ut.edu', 'United Kingdom', 'timothymitchell', 'Timothy', 'Mitchell', 'M'),
       ('Integer@loremeu.edu', 'Germany', 'merrittbecker', 'Merritt', 'Becker', 'M'),
       ('eget@nisl.org', 'Germany', 'griffithgay', 'Griffith', 'Gay', 'M'),
       ('lorem@tortoratrisus.org', 'United Kingdom', 'lewisrandolph', 'Lewis', 'Randolph', 'M'),
       ('tempus@ac.com', 'United Kingdom', 'wyattdaniels', 'Wyatt', 'Daniels', 'M'),
       ('vestibulum.neque.sed@imperdietullamcorperDuis.co.uk', 'The Netherlands', 'hedleychaney', 'Hedley', 'Chaney',
        'M'),
       ('urna.Nunc@lobortis.org', 'The Netherlands', 'gilchavez', 'Gil', 'Chavez', 'M'),
       ('at.arcu@Aliquamornarelibero.org', 'The Netherlands', 'rajahubbard', 'Raja', 'Hubbard', 'M'),
       ('condimentum.eget.volutpat@eleifend.net', 'Germany', 'ulyssesferguson', 'Ulysses', 'Ferguson', 'M'),
       ('Praesent.eu@semper.ca', 'Germany', 'nicholassherman', 'Nicholas', 'Sherman', 'M'),
       ('in@justo.edu', 'Germany', 'reedchaney', 'Reed', 'Chaney', 'M'),
       ('eu.tellus.Phasellus@nonhendreritid.com', 'The Netherlands', 'dustinwhite', 'Dustin', 'White', 'M'),
       ('odio.Etiam@nonloremvitae.edu', 'The Netherlands', 'lamarsnow', 'Lamar', 'Snow', 'M'),
       ('aliquam.iaculis.lacus@eleifendnecmalesuada.net', 'Germany', 'dennisflowers', 'Dennis', 'Flowers', 'M'),
       ('sed.consequat.auctor@rhoncusid.edu', 'The Netherlands', 'hophaley', 'Hop', 'Haley', 'M'),
       ('orci@enimEtiam.edu', 'United Kingdom', 'jonasrosario', 'Jonas', 'Rosario', 'M'),
       ('amet.risus@Maecenasmifelis.org', 'Germany', 'huanthony', 'Hu', 'Anthony', 'M'),
       ('Nulla@Loremipsum.ca', 'Germany', 'chadwickclemons', 'Chadwick', 'Clemons', 'M'),
       ('sapien@egetnisidictum.net', 'United Kingdom', 'brennanroach', 'Brennan', 'Roach', 'M'),
       ('dolor.dolor.tempus@penatibus.com', 'Germany', 'rudyardarmstrong', 'Rudyard', 'Armstrong', 'M'),
       ('amet.metus@Aliquamerat.edu', 'The Netherlands', 'devindudley', 'Devin', 'Dudley', 'M'),
       ('Ut.nec@tinciduntaliquam.com', 'Germany', 'wangcortez', 'Wang', 'Cortez', 'M'),
       ('facilisi@afeugiat.edu', 'The Netherlands', 'leophillips', 'Leo', 'Phillips', 'M'),
       ('sagittis.felis@velfaucibus.com', 'United Kingdom', 'jermaineyoung', 'Jermaine', 'Young', 'M'),
       ('Integer@mauris.co.uk', 'Germany', 'malikrocha', 'Malik', 'Rocha', 'M'),
       ('vestibulum.nec.euismod@Maecenas.edu', 'United Kingdom', 'russellmorgan', 'Russell', 'Morgan', 'M'),
       ('non.egestas@dolor.co.uk', 'The Netherlands', 'simonbrennan', 'Simon', 'Brennan', 'M'),
       ('vitae.velit.egestas@nonhendrerit.edu', 'Germany', 'tatecain', 'Tate', 'Cain', 'M'),
       ('malesuada.ut@nulla.net', 'United Kingdom', 'dentonharrison', 'Denton', 'Harrison', 'M'),
       ('non.lacinia@quisurna.org', 'The Netherlands', 'andrewmarks', 'Andrew', 'Marks', 'M'),
       ('vel.turpis.Aliquam@massalobortisultrices.ca', 'Germany', 'knoxpeck', 'Knox', 'Peck', 'M'),
       ('ante.lectus.convallis@sodalesMauris.ca', 'The Netherlands', 'baxterwood', 'Baxter', 'Wood', 'M'),
       ('Nunc.pulvinar@Duisgravida.edu', 'The Netherlands', 'edanjefferson', 'Edan', 'Jefferson', 'M'),
       ('Duis@Curabiturvellectus.edu', 'Germany', 'lamaravila', 'Lamar', 'Avila', 'M'),
       ('Nullam.lobortis.quam@lobortisauguescelerisque.com', 'The Netherlands', 'nissimvaughan', 'Nissim', 'Vaughan',
        'M'),
       ('mi.Aliquam@Curabitur.com', 'Germany', 'claytonroach', 'Clayton', 'Roach', 'M'),
       ('tellus@dictum.co.uk', 'United Kingdom', 'luciancarey', 'Lucian', 'Carey', 'M'),
       ('et.risus.Quisque@adipiscing.net', 'United Kingdom', 'ardenmeyers', 'Arden', 'Meyers', 'M'),
       ('dolor.sit.amet@tempor.com', 'Germany', 'davidgentry', 'David', 'Gentry', 'M'),
       ('amet.lorem.semper@at.net', 'United Kingdom', 'amoscarlson', 'Amos', 'Carlson', 'M'),
       ('ut@elit.net', 'United Kingdom', 'troythornton', 'Troy', 'Thornton', 'M'),
       ('leo.elementum@Nullamsuscipitest.com', 'Germany', 'keatonleblanc', 'Keaton', 'Leblanc', 'M'),
       ('sollicitudin@etmalesuadafames.ca', 'The Netherlands', 'dillonmonroe', 'Dillon', 'Monroe', 'M'),
       ('Suspendisse.aliquet.molestie@dolor.net', 'United Kingdom', 'clintondunlap', 'Clinton', 'Dunlap', 'M'),
       ('penatibus.et.magnis@vitaesodales.co.uk', 'Germany', 'wyliemccarty', 'Wylie', 'Mccarty', 'M'),
       ('magna@vitae.ca', 'The Netherlands', 'leonardlove', 'Leonard', 'Love', 'M'),
       ('non.nisi@interdum.edu', 'The Netherlands', 'harpercherry', 'Harper', 'Cherry', 'M'),
       ('ligula.Aliquam@orci.ca', 'Germany', 'chesterquinn', 'Chester', 'Quinn', 'M'),
       ('in@nunc.com', 'United Kingdom', 'yardleybarber', 'Yardley', 'Barber', 'M'),
       ('eget.volutpat.ornare@pharetra.org', 'United Kingdom', 'dustinbright', 'Dustin', 'Bright', 'M'),
       ('id.mollis.nec@parturient.net', 'The Netherlands', 'ciarancochran', 'Ciaran', 'Cochran', 'M'),
       ('pellentesque.a@risusDonec.edu', 'Germany', 'rashadpaul', 'Rashad', 'Paul', 'M'),
       ('sit@ipsumprimis.co.uk', 'United Kingdom', 'prestonvelasquez', 'Preston', 'Velasquez', 'M'),
       ('pede.Suspendisse@lacus.co.uk', 'United Kingdom', 'micahcochran', 'Micah', 'Cochran', 'M'),
       ('mollis.Duis.sit@risusquis.org', 'Germany', 'maconguzman', 'Macon', 'Guzman', 'M'),
       ('dolor.sit@fringillapurus.edu', 'Germany', 'nigelruiz', 'Nigel', 'Ruiz', 'M'),
       ('Donec.vitae@quam.com', 'United Kingdom', 'talonconway', 'Talon', 'Conway', 'M'),
       ('Morbi.non@Suspendisse.ca', 'United Kingdom', 'kennethbarr', 'Kenneth', 'Barr', 'M'),
       ('sem@quam.edu', 'Germany', 'valentinefarrell', 'Valentine', 'Farrell', 'M'),
       ('nunc.est.mollis@porttitortellus.org', 'Germany', 'akeempacheco', 'Akeem', 'Pacheco', 'M'),
       ('cursus.et@placerataugue.net', 'United Kingdom', 'sebastiansimmons', 'Sebastian', 'Simmons', 'M'),
       ('lorem@malesuada.org', 'The Netherlands', 'jakeemkline', 'Jakeem', 'Kline', 'M'),
       ('neque.Nullam@In.net', 'The Netherlands', 'toddmack', 'Todd', 'Mack', 'M'),
       ('elementum.purus.accumsan@necurnasuscipit.org', 'Germany', 'stewartferrell', 'Stewart', 'Ferrell', 'M'),
       ('mi.enim.condimentum@nisi.ca', 'The Netherlands', 'gabrielrice', 'Gabriel', 'Rice', 'M'),
       ('Proin.vel.nisl@loremauctor.com', 'Germany', 'leonardleach', 'Leonard', 'Leach', 'M'),
       ('scelerisque.neque.sed@Pellentesque.co.uk', 'United Kingdom', 'flynnrocha', 'Flynn', 'Rocha', 'M'),
       ('Phasellus.ornare@nuncinterdum.com', 'The Netherlands', 'raphaelmccarty', 'Raphael', 'Mccarty', 'M'),
       ('tortor.at.risus@eu.net', 'Germany', 'camdenchandler', 'Camden', 'Chandler', 'M'),
       ('facilisis.lorem@dolordapibus.org', 'Germany', 'toddwood', 'Todd', 'Wood', 'M'),
       ('erat@euaccumsan.com', 'Germany', 'caldwellmccarty', 'Caldwell', 'Mccarty', 'M'),
       ('lacinia@arcu.ca', 'United Kingdom', 'eltonmorin', 'Elton', 'Morin', 'M'),
       ('nisi.Mauris@mattisCraseget.org', 'Germany', 'kevinpowers', 'Kevin', 'Powers', 'M'),
       ('Aenean.sed@odio.ca', 'The Netherlands', 'masonhuffman', 'Mason', 'Huffman', 'M'),
       ('malesuada.ut.sem@loremipsum.co.uk', 'Germany', 'hoytnolan', 'Hoyt', 'Nolan', 'M'),
       ('justo.sit.amet@nislMaecenas.co.uk', 'The Netherlands', 'fultonfrank', 'Fulton', 'Frank', 'M'),
       ('Curabitur.sed.tortor@augue.co.uk', 'Germany', 'malikherman', 'Malik', 'Herman', 'M'),
       ('faucibus@vestibulumloremsit.com', 'Germany', 'isaachood', 'Isaac', 'Hood', 'M'),
       ('viverra@sagittisplaceratCras.net', 'The Netherlands', 'avramjennings', 'Avram', 'Jennings', 'M'),
       ('nec.urna.suscipit@Proinvelit.net', 'Germany', 'abdullopez', 'Abdul', 'Lopez', 'M'),
       ('egestas.rhoncus.Proin@vitaeposuereat.org', 'The Netherlands', 'abrahampetersen', 'Abraham', 'Petersen', 'M'),
       ('dictum.Proin@urnaNullam.net', 'The Netherlands', 'basiljuarez', 'Basil', 'Juarez', 'M'),
       ('justo@Donecfelisorci.net', 'The Netherlands', 'mufutauclay', 'Mufutau', 'Clay', 'M'),
       ('nisl@euismodest.edu', 'The Netherlands', 'talonneal', 'Talon', 'Neal', 'M'),
       ('vel.est.tempor@semutcursus.edu', 'Germany', 'hiramwalters', 'Hiram', 'Walters', 'M'),
       ('non.luctus@Integersemelit.ca', 'United Kingdom', 'hyatthardy', 'Hyatt', 'Hardy', 'M');
GO

-----------------------
-- Subscription_Type
-----------------------
INSERT INTO odisee1.dbo.Subscription_Type (subscription_type_name)
VALUES ('Basic'),
       ('Premium'),
       ('Pro');
GO
-----------------------
-- Subscription
-----------------------
INSERT INTO odisee1.dbo.Subscription (country_name, subscription_type, monthly_fee, discount_percentage)
VALUES ('The Netherlands', 'Basic', 4.00, 0.00),
       ('The Netherlands', 'Premium', 5.00, 20.00),
       ('The Netherlands', 'Pro', 6.00, 40.00),
       ('Germany', 'Basic', 3.50, 0.00),
       ('Germany', 'Premium', 5.00, 20.00),
       ('Germany', 'Pro', 6.50, 40.00),
       ('United Kingdom', 'Basic', 3.50, 0.00),
       ('United Kingdom', 'Premium', 4.50, 20.00),
       ('United Kingdom', 'Pro', 5.50, 40.00);

-----------------------
-- UserSubscription
-----------------------
INSERT INTO odisee1.dbo.User_Subscription (email_address, country_name, subscription_type_name, subscription_startdate,
                                          subscription_enddate, monthly_fee)
VALUES ('Integer@loremeu.edu', 'Germany', 'Premium', '2018-04-12', '2019-04-05', 5.00),
       ('joey.stoffels@gmail.com', 'The Netherlands', 'Pro', '2019-04-01', '2019-05-01', 6.00),
       ('nickhartjes@gmail.com', 'The Netherlands', 'Pro', '2019-04-01', '2019-05-01', 6.00),
       ('Nam.tempor@ut.edu', 'United Kingdom', 'Basic', '2019-01-01', null, 3.50),
       ('hank.schrader@dea.gov', 'United Kingdom', 'Basic', '2019-01-01', null, 3.50);


-----------------------
-- Purchase
-----------------------
INSERT INTO odisee1.dbo.Purchase (product_id, email_address, purchase_date, price)
VALUES (754, 'hank.schrader@dea.gov', '2019-04-02', 1.00),
       (830, 'hank.schrader@dea.gov', '2019-04-03', 1.00),
       (1077, 'hank.schrader@dea.gov', '2019-04-01', 1.00),
       (412372, 'nickhartjes@gmail.com', '2019-04-05', 1.20),
       (412402, 'joey.stoffels@gmail.com', '2019-04-16', 1.20),
       (412404, 'joey.stoffels@gmail.com', '2019-04-02', 1.20),
       (412457, 'nickhartjes@gmail.com', '2019-04-05', 1.20),
       (412460, 'nickhartjes@gmail.com', '2019-04-05', 1.20),
       (412464, 'nickhartjes@gmail.com', '2019-04-05', 1.20),
       (412597, 'joey.stoffels@gmail.com', '2019-04-17', 1.20),
       (412610, 'joey.stoffels@gmail.com', '2019-04-05', 1.20);
GO

-----------------------
-- Category
-----------------------
INSERT INTO odisee1.dbo.Category(category_name)
VALUES ( 'Plot'), ('Acting'), ('Cinematography'), ('Music and Sound');
GO