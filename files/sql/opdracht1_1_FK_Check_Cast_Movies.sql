-----------------------
-- Negative selection query
-----------------------
SELECT C.*
FROM dbo.Imported_Cast AS C
WHERE C.Mid NOT IN (
  SELECT M.Id
  FROM dbo.Imported_Movie AS M
);
-- Time: na 7 minuten uitgezet

SELECT C.*
FROM dbo.Imported_Cast AS C
       LEFT JOIN
     dbo.Imported_Movie AS M
     ON C.Mid = M.Id
WHERE M.Id IS NULL
-- Time 0:04 0 Records

-----------------------
-- Delete
-----------------------
DELETE C
FROM dbo.Imported_Cast AS C
         LEFT JOIN
     dbo.Imported_Movie AS M
     ON C.Mid = M.Id
WHERE M.Id IS NULL
-- 0 rows affected in 5 s 489 ms