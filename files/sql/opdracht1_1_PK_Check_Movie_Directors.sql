-----------------------
-- Negative selection query
-----------------------
SELECT MD.Did, MD.Mid
FROM dbo.Imported_Movie_Directors AS MD
GROUP BY MD.Did, MD.Mid
HAVING COUNT(*) > 1;
-- 0 rows retrieved in 614 ms (execution: 574 ms, fetching: 40 ms)

-----------------------
-- Remove duplicates except one
-----------------------
WITH duplicates AS
         (
             SELECT Did, Mid, ROW_NUMBER() OVER (PARTITION BY Did, Mid ORDER BY Did, Mid) RowNumber
             FROM dbo.Imported_Movie_Directors
         )
DELETE
FROM duplicates
WHERE RowNumber > 1
-- completed in 1 s 845 ms
GO

-----------------------
-- Test set negative selection query
-----------------------
DECLARE
    @mid varchar(10);
DECLARE
    @did varchar(10);
SET @mid = 1;
SET @did = 1;

-- Check base
SELECT COUNT(*)
FROM dbo.Imported_Movie_Directors AS MD
WHERE MD.Did = @did
  AND MD.Mid = @mid;
-- Result = 0

-- Insert test set
INSERT INTO dbo.Imported_Movie_Directors (Did, Mid)
VALUES (@did, @mid),
       (@did, @mid);

-- Check inserted data
SELECT COUNT(*)
FROM dbo.Imported_Movie_Directors AS MD
WHERE MD.Did = @did
  AND MD.Mid = @mid;
-- Result = 2

-- Now run the negative selection query

-- Remove test data
DELETE
FROM dbo.Imported_Movie_Directors
WHERE Mid = @mid
  AND Did = @did;