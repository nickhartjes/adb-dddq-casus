/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     4/5/2019 9:04:50 PM                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Cast') and o.name = 'FK_CAST_CAST_MEMB_PERSON')
alter table Cast
   drop constraint FK_CAST_CAST_MEMB_PERSON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Cast') and o.name = 'FK_CAST_CAST_OF_M_PRODUCT')
alter table Cast
   drop constraint FK_CAST_CAST_OF_M_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Country') and o.name = 'FK_COUNTRY_REFERENCE_COUNTRY_')
alter table Country
   drop constraint FK_COUNTRY_REFERENCE_COUNTRY_
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Default_Game_Price') and o.name = 'FK_DEFAULT__COUNTRYDE_COUNTRY')
alter table Default_Game_Price
   drop constraint FK_DEFAULT__COUNTRYDE_COUNTRY
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Default_Game_Price') and o.name = 'FK_DEFAULT__GAMEDEFAU_PRODUCT')
alter table Default_Game_Price
   drop constraint FK_DEFAULT__GAMEDEFAU_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Invoice') and o.name = 'FK_INVOICE_USER_OF_I_USER')
alter table Invoice
   drop constraint FK_INVOICE_USER_OF_I_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Movie_Director') and o.name = 'FK_MOVIE_DI_DIRECTS_PRODUCT')
alter table Movie_Director
   drop constraint FK_MOVIE_DI_DIRECTS_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Movie_Director') and o.name = 'FK_MOVIE_DI_IS_DIRECT_PERSON')
alter table Movie_Director
   drop constraint FK_MOVIE_DI_IS_DIRECT_PERSON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Product') and o.name = 'FK_PRODUCT_REFERENCE_PRODUCT_')
alter table Product
   drop constraint FK_PRODUCT_REFERENCE_PRODUCT_
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Product') and o.name = 'FK_PRODUCT_PREVIOUS__PRODUCT')
alter table Product
   drop constraint FK_PRODUCT_PREVIOUS__PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Product_Genre') and o.name = 'FK_PRODUCT__PRODUCT_G_PRODUCT')
alter table Product_Genre
   drop constraint FK_PRODUCT__PRODUCT_G_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Product_Genre') and o.name = 'FK_PRODUCT__IS OF GEN_GENRE')
alter table Product_Genre
   drop constraint "FK_PRODUCT__IS OF GEN_GENRE"
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Purchase') and o.name = 'FK_PURCHASE_PRODUCT_O_PRODUCT')
alter table Purchase
   drop constraint FK_PURCHASE_PRODUCT_O_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Purchase') and o.name = 'FK_PURCHASE_USER_OF_P_USER')
alter table Purchase
   drop constraint FK_PURCHASE_USER_OF_P_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Review') and o.name = 'FK_REVIEW_PRODUCT_O_PRODUCT')
alter table Review
   drop constraint FK_REVIEW_PRODUCT_O_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Review') and o.name = 'FK_REVIEW_USER_OF_R_USER')
alter table Review
   drop constraint FK_REVIEW_USER_OF_R_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Review_Category') and o.name = 'FK_REVIEW_C_REVIEWCAT_CATEGORY')
alter table Review_Category
   drop constraint FK_REVIEW_C_REVIEWCAT_CATEGORY
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Review_Category') and o.name = 'FK_REVIEW_C_REVIEWCAT_REVIEW')
alter table Review_Category
   drop constraint FK_REVIEW_C_REVIEWCAT_REVIEW
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Subscription') and o.name = 'FK_SUBSCRIP_REFERENCE_SUBSCRIP')
alter table Subscription
   drop constraint FK_SUBSCRIP_REFERENCE_SUBSCRIP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Subscription') and o.name = 'FK_SUBSCRIP_RELATIONS_COUNTRY')
alter table Subscription
   drop constraint FK_SUBSCRIP_RELATIONS_COUNTRY
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"User"') and o.name = 'FK_USER_USERCOUNT_COUNTRY')
alter table "User"
   drop constraint FK_USER_USERCOUNT_COUNTRY
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('User_Subscription') and o.name = 'FK_USER_SUB_SUBSCRIPT_SUBSCRIP')
alter table User_Subscription
   drop constraint FK_USER_SUB_SUBSCRIPT_SUBSCRIP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('User_Subscription') and o.name = 'FK_USER_SUB_USER_OF_U_USER')
alter table User_Subscription
   drop constraint FK_USER_SUB_USER_OF_U_USER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Cast')
            and   name  = 'CAST_OF_MOVIE_FK'
            and   indid > 0
            and   indid < 255)
   drop index Cast.CAST_OF_MOVIE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Cast')
            and   name  = 'CAST_MEMBER_OF_CAST_FK'
            and   indid > 0
            and   indid < 255)
   drop index Cast.CAST_MEMBER_OF_CAST_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Cast')
            and   type = 'U')
   drop table Cast
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Category')
            and   type = 'U')
   drop table Category
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Country')
            and   type = 'U')
   drop table Country
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Country_Currency')
            and   type = 'U')
   drop table Country_Currency
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Default_Game_Price')
            and   name  = 'GAMEDEFAULTGAMEPRICE_FK'
            and   indid > 0
            and   indid < 255)
   drop index Default_Game_Price.GAMEDEFAULTGAMEPRICE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Default_Game_Price')
            and   name  = 'COUNTRYDEFAULTGAMEPRICE_FK'
            and   indid > 0
            and   indid < 255)
   drop index Default_Game_Price.COUNTRYDEFAULTGAMEPRICE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Default_Game_Price')
            and   type = 'U')
   drop table Default_Game_Price
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Genre')
            and   type = 'U')
   drop table Genre
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Invoice')
            and   name  = 'USER_OF_INVOICE_FK'
            and   indid > 0
            and   indid < 255)
   drop index Invoice.USER_OF_INVOICE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Invoice')
            and   type = 'U')
   drop table Invoice
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Movie_Director')
            and   name  = 'IS_DIRECTED_BY_FK'
            and   indid > 0
            and   indid < 255)
   drop index Movie_Director.IS_DIRECTED_BY_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Movie_Director')
            and   name  = 'DIRECTS_FK'
            and   indid > 0
            and   indid < 255)
   drop index Movie_Director.DIRECTS_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Movie_Director')
            and   type = 'U')
   drop table Movie_Director
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Person')
            and   type = 'U')
   drop table Person
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Product')
            and   name  = 'IS_PREVIOUS_PART_OF_FK'
            and   indid > 0
            and   indid < 255)
   drop index Product.IS_PREVIOUS_PART_OF_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Product')
            and   type = 'U')
   drop table Product
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Product_Genre')
            and   name  = 'IS_OF_GENRE_FK'
            and   indid > 0
            and   indid < 255)
   drop index Product_Genre.IS_OF_GENRE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Product_Genre')
            and   name  = 'PRODUCT_GENRE_FK'
            and   indid > 0
            and   indid < 255)
   drop index Product_Genre.PRODUCT_GENRE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Product_Genre')
            and   type = 'U')
   drop table Product_Genre
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Product_Type')
            and   type = 'U')
   drop table Product_Type
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Purchase')
            and   name  = 'USER_OF_PURCHASE_FK'
            and   indid > 0
            and   indid < 255)
   drop index Purchase.USER_OF_PURCHASE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Purchase')
            and   name  = 'PRODUCT_OF_PURCHASE_FK'
            and   indid > 0
            and   indid < 255)
   drop index Purchase.PRODUCT_OF_PURCHASE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Purchase')
            and   type = 'U')
   drop table Purchase
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Review')
            and   name  = 'USER_OF_REVIEW_FK'
            and   indid > 0
            and   indid < 255)
   drop index Review.USER_OF_REVIEW_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Review')
            and   name  = 'PRODUCT_OF_REVIEW_FK'
            and   indid > 0
            and   indid < 255)
   drop index Review.PRODUCT_OF_REVIEW_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Review')
            and   type = 'U')
   drop table Review
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Review_Category')
            and   name  = 'REVIEWCATEGORY_FK2'
            and   indid > 0
            and   indid < 255)
   drop index Review_Category.REVIEWCATEGORY_FK2
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Review_Category')
            and   name  = 'REVIEWCATEGORY_FK'
            and   indid > 0
            and   indid < 255)
   drop index Review_Category.REVIEWCATEGORY_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Review_Category')
            and   type = 'U')
   drop table Review_Category
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Subscription')
            and   name  = 'RELATIONSHIP_6_FK'
            and   indid > 0
            and   indid < 255)
   drop index Subscription.RELATIONSHIP_6_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Subscription')
            and   type = 'U')
   drop table Subscription
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Subscription_Type')
            and   type = 'U')
   drop table Subscription_Type
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('"User"')
            and   name  = 'USERCOUNTRY_FK'
            and   indid > 0
            and   indid < 255)
   drop index "User".USERCOUNTRY_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"User"')
            and   type = 'U')
   drop table "User"
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('User_Subscription')
            and   name  = 'SUBSCRIPTION_OF_USERSUBSCRIPTION_FK'
            and   indid > 0
            and   indid < 255)
   drop index User_Subscription.SUBSCRIPTION_OF_USERSUBSCRIPTION_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('User_Subscription')
            and   name  = 'USER_OF_USERSUBSCRIPTION_FK'
            and   indid > 0
            and   indid < 255)
   drop index User_Subscription.USER_OF_USERSUBSCRIPTION_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('User_Subscription')
            and   type = 'U')
   drop table User_Subscription
go

if exists(select 1 from systypes where name='AMOUNT')
   drop type AMOUNT
go

if exists(select 1 from systypes where name='CATEGORY')
   drop type CATEGORY
go

if exists(select 1 from systypes where name='COUNTRY')
   drop type COUNTRY
go

if exists(select 1 from systypes where name='COVER_IMAGE')
   drop type COVER_IMAGE
go

if exists(select 1 from systypes where name='CURRENCY')
   drop type CURRENCY
go

if exists(select 1 from systypes where name='DATE')
   drop type DATE
go

if exists(select 1 from systypes where name='DESCRIPTION')
   drop type DESCRIPTION
go

if exists(select 1 from systypes where name='DURATION')
   drop type DURATION
go

if exists(select 1 from systypes where name='EMAIL')
   drop type EMAIL
go

if exists(select 1 from systypes where name='FIRST_NAME')
   drop type FIRST_NAME
go

if exists(select 1 from systypes where name='GENDER')
   drop type GENDER
go

if exists(select 1 from systypes where name='GENRE')
   drop type GENRE
go

if exists(select 1 from systypes where name='ID')
   drop type ID
go

if exists(select 1 from systypes where name='LAST_NAME')
   drop type LAST_NAME
go

if exists(select 1 from systypes where name='MONTH_YEAR')
   drop type MONTH_YEAR
go

if exists(select 1 from systypes where name='NUMBER')
   drop type NUMBER
go

if exists(select 1 from systypes where name='PAYMENT_METHOD')
   drop type PAYMENT_METHOD
go

if exists(select 1 from systypes where name='PERCENTAGE')
   drop type PERCENTAGE
go

if exists(select 1 from systypes where name='PREVIOUS_PART')
   drop type PREVIOUS_PART
go

if exists(select 1 from systypes where name='PRICE')
   drop type PRICE
go

if exists(select 1 from systypes where name='ROLE')
   drop type ROLE
go

if exists(select 1 from systypes where name='SCORE')
   drop type SCORE
go

if exists(select 1 from systypes where name='TITLE')
   drop type TITLE
go

if exists(select 1 from systypes where name='TYPE')
   drop type TYPE
go

if exists(select 1 from systypes where name='URL')
   drop type URL
go

if exists(select 1 from systypes where name='USERNAME')
   drop type USERNAME
go

if exists(select 1 from systypes where name='YEAR')
   drop type YEAR
go

/*==============================================================*/
/* Domain: AMOUNT                                               */
/*==============================================================*/
create type AMOUNT
   from numeric(8,2)
go

/*==============================================================*/
/* Domain: CATEGORY                                             */
/*==============================================================*/
create type CATEGORY
   from varchar(255)
go

/*==============================================================*/
/* Domain: COUNTRY                                              */
/*==============================================================*/
create type COUNTRY
   from varchar(255)
go

/*==============================================================*/
/* Domain: COVER_IMAGE                                          */
/*==============================================================*/
create type COVER_IMAGE
   from varchar(255)
go

/*==============================================================*/
/* Domain: CURRENCY                                             */
/*==============================================================*/
create type CURRENCY
   from varchar(255)
go

/*==============================================================*/
/* Domain: DATE                                                 */
/*==============================================================*/
create type DATE
   from datetime
go

/*==============================================================*/
/* Domain: DESCRIPTION                                          */
/*==============================================================*/
create type DESCRIPTION
   from varchar(255)
go

/*==============================================================*/
/* Domain: DURATION                                             */
/*==============================================================*/
create type DURATION
   from int
go

/*==============================================================*/
/* Domain: EMAIL                                                */
/*==============================================================*/
create type EMAIL
   from varchar(255)
go

/*==============================================================*/
/* Domain: FIRST_NAME                                           */
/*==============================================================*/
create type FIRST_NAME
   from varchar(50)
go

/*==============================================================*/
/* Domain: GENDER                                               */
/*==============================================================*/
create type GENDER
   from char(1)
go

/*==============================================================*/
/* Domain: GENRE                                                */
/*==============================================================*/
create type GENRE
   from varchar(255)
go

/*==============================================================*/
/* Domain: ID                                                   */
/*==============================================================*/
create type ID
   from int
go

/*==============================================================*/
/* Domain: LAST_NAME                                            */
/*==============================================================*/
create type LAST_NAME
   from varchar(50)
go

/*==============================================================*/
/* Domain: MONTH_YEAR                                           */
/*==============================================================*/
create type MONTH_YEAR
   from datetime
go

/*==============================================================*/
/* Domain: NUMBER                                               */
/*==============================================================*/
create type NUMBER
   from numeric
go

/*==============================================================*/
/* Domain: PAYMENT_METHOD                                       */
/*==============================================================*/
create type PAYMENT_METHOD
   from varchar(255)
go

/*==============================================================*/
/* Domain: PERCENTAGE                                           */
/*==============================================================*/
create type PERCENTAGE
   from numeric(5,2)
go

/*==============================================================*/
/* Domain: PREVIOUS_PART                                        */
/*==============================================================*/
create type PREVIOUS_PART
   from int
go

/*==============================================================*/
/* Domain: PRICE                                                */
/*==============================================================*/
create type PRICE
   from numeric(5,2)
go

/*==============================================================*/
/* Domain: ROLE                                                 */
/*==============================================================*/
create type ROLE
   from varchar(255)
go

/*==============================================================*/
/* Domain: SCORE                                                */
/*==============================================================*/
create type SCORE
   from int
go

/*==============================================================*/
/* Domain: TITLE                                                */
/*==============================================================*/
create type TITLE
   from varchar(255)
go

/*==============================================================*/
/* Domain: TYPE                                                 */
/*==============================================================*/
create type TYPE
   from varchar(255)
go

/*==============================================================*/
/* Domain: URL                                                  */
/*==============================================================*/
create type URL
   from varchar(255)
go

/*==============================================================*/
/* Domain: USERNAME                                             */
/*==============================================================*/
create type USERNAME
   from varchar(255)
go

/*==============================================================*/
/* Domain: YEAR                                                 */
/*==============================================================*/
create type YEAR
   from int
go

/*==============================================================*/
/* Table: Cast                                                  */
/*==============================================================*/
create table Cast (
   person_id            ID                   not null,
   product_id           ID                   not null,
   role                 ROLE                 not null,
   constraint PK_CAST primary key nonclustered (person_id, product_id, role)
)
go

/*==============================================================*/
/* Index: CAST_MEMBER_OF_CAST_FK                                */
/*==============================================================*/
create index CAST_MEMBER_OF_CAST_FK on Cast (
person_id ASC
)
go

/*==============================================================*/
/* Index: CAST_OF_MOVIE_FK                                      */
/*==============================================================*/
create index CAST_OF_MOVIE_FK on Cast (
product_id ASC
)
go

/*==============================================================*/
/* Table: Category                                              */
/*==============================================================*/
create table Category (
   category_name        CATEGORY             not null,
   constraint PK_CATEGORY primary key nonclustered (category_name)
)
go

/*==============================================================*/
/* Table: Country                                               */
/*==============================================================*/
create table Country (
   country_name         COUNTRY              not null,
   country_currency     varchar(255)         not null,
   constraint PK_COUNTRY primary key nonclustered (country_name)
)
go

/*==============================================================*/
/* Table: Country_Currency                                      */
/*==============================================================*/
create table Country_Currency (
   country_currency_name varchar(255)         not null,
   constraint PK_COUNTRY_CURRENCY primary key (country_currency_name)
)
go

/*==============================================================*/
/* Table: Default_Game_Price                                    */
/*==============================================================*/
create table Default_Game_Price (
   country_name         COUNTRY              not null,
   product_id           ID                   not null,
   default_price        PRICE                not null,
   constraint PK_DEFAULT_GAME_PRICE primary key (country_name, product_id)
)
go

/*==============================================================*/
/* Index: COUNTRYDEFAULTGAMEPRICE_FK                            */
/*==============================================================*/
create index COUNTRYDEFAULTGAMEPRICE_FK on Default_Game_Price (
country_name ASC
)
go

/*==============================================================*/
/* Index: GAMEDEFAULTGAMEPRICE_FK                               */
/*==============================================================*/
create index GAMEDEFAULTGAMEPRICE_FK on Default_Game_Price (
product_id ASC
)
go

/*==============================================================*/
/* Table: Genre                                                 */
/*==============================================================*/
create table Genre (
   genre_name           GENRE                not null,
   constraint PK_GENRE primary key nonclustered (genre_name)
)
go

/*==============================================================*/
/* Table: Invoice                                               */
/*==============================================================*/
create table Invoice (
   email_address        EMAIL                not null,
   invoice_month_year   MONTH_YEAR           not null,
   subtotal             AMOUNT               not null,
   total_price          AMOUNT               not null,
   constraint PK_INVOICE primary key nonclustered (email_address, invoice_month_year)
)
go

/*==============================================================*/
/* Index: USER_OF_INVOICE_FK                                    */
/*==============================================================*/
create index USER_OF_INVOICE_FK on Invoice (
email_address ASC
)
go

/*==============================================================*/
/* Table: Movie_Director                                        */
/*==============================================================*/
create table Movie_Director (
   product_id           ID                   not null,
   person_id            ID                   not null,
   constraint PK_MOVIE_DIRECTOR primary key (product_id, person_id)
)
go

/*==============================================================*/
/* Index: DIRECTS_FK                                            */
/*==============================================================*/
create index DIRECTS_FK on Movie_Director (
product_id ASC
)
go

/*==============================================================*/
/* Index: IS_DIRECTED_BY_FK                                     */
/*==============================================================*/
create index IS_DIRECTED_BY_FK on Movie_Director (
person_id ASC
)
go

/*==============================================================*/
/* Table: Person                                                */
/*==============================================================*/
create table Person (
   person_id            ID                   not null,
   last_name            LAST_NAME            not null,
   first_name           FIRST_NAME           not null,
   gender               GENDER               null,
   constraint PK_PERSON primary key nonclustered (person_id)
)
go

/*==============================================================*/
/* Table: Product                                               */
/*==============================================================*/
create table Product (
   product_id           ID                   not null,
   product_type         varchar(255)         not null,
   previous_product_id  ID                   null,
   title                TITLE                not null,
   cover_image          COVER_IMAGE          null,
   description          DESCRIPTION          null,
   movie_default_price  PRICE                not null,
   publication_year     YEAR                 null,
   number_of_online_players NUMBER               null,
   duration             DURATION             null,
   url                  URL                  null,
   constraint PK_PRODUCT primary key nonclustered (product_id)
)
go

/*==============================================================*/
/* Index: IS_PREVIOUS_PART_OF_FK                                */
/*==============================================================*/
create index IS_PREVIOUS_PART_OF_FK on Product (
previous_product_id ASC
)
go

/*==============================================================*/
/* Table: Product_Genre                                         */
/*==============================================================*/
create table Product_Genre (
   product_id           ID                   not null,
   genre_name           GENRE                not null,
   constraint PK_PRODUCT_GENRE primary key (product_id, genre_name)
)
go

/*==============================================================*/
/* Index: PRODUCT_GENRE_FK                                      */
/*==============================================================*/
create index PRODUCT_GENRE_FK on Product_Genre (
product_id ASC
)
go

/*==============================================================*/
/* Index: IS_OF_GENRE_FK                                        */
/*==============================================================*/
create index IS_OF_GENRE_FK on Product_Genre (
genre_name ASC
)
go

/*==============================================================*/
/* Table: Product_Type                                          */
/*==============================================================*/
create table Product_Type (
   product_type_name    varchar(255)         not null,
   constraint PK_PRODUCT_TYPE primary key (product_type_name)
)
go

/*==============================================================*/
/* Table: Purchase                                              */
/*==============================================================*/
create table Purchase (
   product_id           ID                   not null,
   email_address        EMAIL                not null,
   purchase_date        DATE                 not null,
   price                PRICE                not null,
   constraint PK_PURCHASE primary key (product_id, email_address)
)
go

/*==============================================================*/
/* Index: PRODUCT_OF_PURCHASE_FK                                */
/*==============================================================*/
create index PRODUCT_OF_PURCHASE_FK on Purchase (
product_id ASC
)
go

/*==============================================================*/
/* Index: USER_OF_PURCHASE_FK                                   */
/*==============================================================*/
create index USER_OF_PURCHASE_FK on Purchase (
email_address ASC
)
go

/*==============================================================*/
/* Table: Review                                                */
/*==============================================================*/
create table Review (
   product_id           ID                   not null,
   email_address        EMAIL                not null,
   review_date          DATE                 not null,
   description          DESCRIPTION          not null,
   average_score        int                  null,
   constraint PK_REVIEW primary key (product_id, email_address)
)
go

/*==============================================================*/
/* Index: PRODUCT_OF_REVIEW_FK                                  */
/*==============================================================*/
create index PRODUCT_OF_REVIEW_FK on Review (
product_id ASC
)
go

/*==============================================================*/
/* Index: USER_OF_REVIEW_FK                                     */
/*==============================================================*/
create index USER_OF_REVIEW_FK on Review (
email_address ASC
)
go

/*==============================================================*/
/* Table: Review_Category                                       */
/*==============================================================*/
create table Review_Category (
   product_id           ID                   not null,
   email_address        EMAIL                not null,
   category_name        CATEGORY             not null,
   score                int                  not null,
   constraint PK_REVIEW_CATEGORY primary key (product_id, email_address, category_name)
)
go

/*==============================================================*/
/* Index: REVIEWCATEGORY_FK                                     */
/*==============================================================*/
create index REVIEWCATEGORY_FK on Review_Category (
product_id ASC,
email_address ASC
)
go

/*==============================================================*/
/* Index: REVIEWCATEGORY_FK2                                    */
/*==============================================================*/
create index REVIEWCATEGORY_FK2 on Review_Category (
category_name ASC
)
go

/*==============================================================*/
/* Table: Subscription                                          */
/*==============================================================*/
create table Subscription (
   country_name         COUNTRY              not null,
   subscription_type    varchar(255)         not null,
   monthly_fee          PRICE                not null,
   discount_percentage  PERCENTAGE           not null,
   constraint PK_SUBSCRIPTION primary key nonclustered (country_name, subscription_type)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_6_FK on Subscription (
country_name ASC
)
go

/*==============================================================*/
/* Table: Subscription_Type                                     */
/*==============================================================*/
create table Subscription_Type (
   subscription_type_name varchar(255)         not null,
   constraint PK_SUBSCRIPTION_TYPE primary key (subscription_type_name)
)
go

/*==============================================================*/
/* Table: "User"                                                */
/*==============================================================*/
create table "User" (
   email_address        EMAIL                not null,
   country_name         COUNTRY              not null,
   username             USERNAME             not null,
   first_name           FIRST_NAME           not null,
   last_name            LAST_NAME            not null,
   gender               GENDER               null,
   birthdate            DATE                 null,
   constraint PK_USER primary key nonclustered (email_address)
)
go

/*==============================================================*/
/* Index: USERCOUNTRY_FK                                        */
/*==============================================================*/
create index USERCOUNTRY_FK on "User" (
country_name ASC
)
go

/*==============================================================*/
/* Table: User_Subscription                                     */
/*==============================================================*/
create table User_Subscription (
   email_address        EMAIL                not null,
   country_name         COUNTRY              not null,
   subscription_type    TYPE                 not null,
   subscription_type_name varchar(255)         not null,
   subscription_startdate DATE                 not null,
   subscription_enddate DATE                 null,
   monthly_fee          PRICE                not null,
   constraint PK_USER_SUBSCRIPTION primary key (country_name, email_address, subscription_type, subscription_type_name, subscription_startdate)
)
go

/*==============================================================*/
/* Index: USER_OF_USERSUBSCRIPTION_FK                           */
/*==============================================================*/
create index USER_OF_USERSUBSCRIPTION_FK on User_Subscription (
email_address ASC
)
go

/*==============================================================*/
/* Index: SUBSCRIPTION_OF_USERSUBSCRIPTION_FK                   */
/*==============================================================*/
create index SUBSCRIPTION_OF_USERSUBSCRIPTION_FK on User_Subscription (
country_name ASC,
subscription_type_name ASC
)
go

alter table Cast
   add constraint FK_CAST_CAST_MEMB_PERSON foreign key (person_id)
      references Person (person_id)
go

alter table Cast
   add constraint FK_CAST_CAST_OF_M_PRODUCT foreign key (product_id)
      references Product (product_id)
go

alter table Country
   add constraint FK_COUNTRY_REFERENCE_COUNTRY_ foreign key (country_currency)
      references Country_Currency (country_currency_name)
go

alter table Default_Game_Price
   add constraint FK_DEFAULT__COUNTRYDE_COUNTRY foreign key (country_name)
      references Country (country_name)
go

alter table Default_Game_Price
   add constraint FK_DEFAULT__GAMEDEFAU_PRODUCT foreign key (product_id)
      references Product (product_id)
go

alter table Invoice
   add constraint FK_INVOICE_USER_OF_I_USER foreign key (email_address)
      references "User" (email_address)
go

alter table Movie_Director
   add constraint FK_MOVIE_DI_DIRECTS_PRODUCT foreign key (product_id)
      references Product (product_id)
go

alter table Movie_Director
   add constraint FK_MOVIE_DI_IS_DIRECT_PERSON foreign key (person_id)
      references Person (person_id)
go

alter table Product
   add constraint FK_PRODUCT_REFERENCE_PRODUCT_ foreign key (product_type)
      references Product_Type (product_type_name)
go

alter table Product
   add constraint FK_PRODUCT_PREVIOUS__PRODUCT foreign key (previous_product_id)
      references Product (product_id)
go

alter table Product_Genre
   add constraint FK_PRODUCT__PRODUCT_G_PRODUCT foreign key (product_id)
      references Product (product_id)
go

alter table Product_Genre
   add constraint "FK_PRODUCT__IS OF GEN_GENRE" foreign key (genre_name)
      references Genre (genre_name)
go

alter table Purchase
   add constraint FK_PURCHASE_PRODUCT_O_PRODUCT foreign key (product_id)
      references Product (product_id)
go

alter table Purchase
   add constraint FK_PURCHASE_USER_OF_P_USER foreign key (email_address)
      references "User" (email_address)
go

alter table Review
   add constraint FK_REVIEW_PRODUCT_O_PRODUCT foreign key (product_id)
      references Product (product_id)
go

alter table Review
   add constraint FK_REVIEW_USER_OF_R_USER foreign key (email_address)
      references "User" (email_address)
go

alter table Review_Category
   add constraint FK_REVIEW_C_REVIEWCAT_CATEGORY foreign key (category_name)
      references Category (category_name)
go

alter table Review_Category
   add constraint FK_REVIEW_C_REVIEWCAT_REVIEW foreign key (product_id, email_address)
      references Review (product_id, email_address)
go

alter table Subscription
   add constraint FK_SUBSCRIP_REFERENCE_SUBSCRIP foreign key (subscription_type)
      references Subscription_Type (subscription_type_name)
go

alter table Subscription
   add constraint FK_SUBSCRIP_RELATIONS_COUNTRY foreign key (country_name)
      references Country (country_name)
go

alter table "User"
   add constraint FK_USER_USERCOUNT_COUNTRY foreign key (country_name)
      references Country (country_name)
go

alter table User_Subscription
   add constraint FK_USER_SUB_SUBSCRIPT_SUBSCRIP foreign key (country_name, subscription_type_name)
      references Subscription (country_name, subscription_type)
go

alter table User_Subscription
   add constraint FK_USER_SUB_USER_OF_U_USER foreign key (email_address)
      references "User" (email_address)
go

