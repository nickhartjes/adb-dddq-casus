-----------------------
-- Negative selection query
-----------------------
SELECT G.*
FROM dbo.Imported_Genre AS G
WHERE G.Id NOT IN (
  SELECT M.Id
  FROM dbo.Imported_Movie AS M
);
-- Time heeel lang

-----------------------
-- Delete
-----------------------
SELECT G.*
FROM dbo.Imported_Genre AS G
         LEFT JOIN
         dbo.Imported_Movie AS M
     ON G.Id = M.Id
WHERE M.Id IS NULL
-- Time 0:00 0 Records
