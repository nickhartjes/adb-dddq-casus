USE odisee;
-----------------------
-- Person tmp
-----------------------

CREATE TABLE Person_tmp
(
  person_id int         NOT NULL,
  lastname  varchar(50) NOT NULL,
  firstname varchar(50) NOT NULL,
  gender    char(1)     NULL,
  PRIMARY KEY (person_id),
  CHECK (gender in ('M', 'F')),
);
GO

-----------------------
-- Person
-----------------------
INSERT INTO odisee.dbo.Person_tmp ( person_id, lastname, firstname, gender)
SELECT  CAST (MP.Id as int) AS Id, 
		CAST(MP.FName AS varchar(50)) AS firstname,
		CAST(MP.LName AS varchar(50)) AS lastname,
		CAST(MP.Gender AS varchar(1)) AS gender
	FROM MYIMDB.dbo.Imported_Person AS MP;
	-- (817718 rows affected)
GO

-----------------------
-- Add Directors ( that don't exists in Person ) in Person and give them an new id
-----------------------
DECLARE @id int;
SELECT @id = max(person_id) + 1 from odisee.dbo.Person_tmp;

INSERT INTO odisee.dbo.Person_tmp(person_id, firstname, lastname)
	SELECT
		ROW_NUMBER() over(order by ID.FName) + @id,
		CAST(ID.FName AS varchar(50)) AS firstname,
		CAST(ID.LName AS varchar(50)) AS lastname
	FROM
	MYIMDB.dbo.Imported_Directors AS ID
	LEFT JOIN MYIMDB.dbo.Imported_Person AS IP ON ID.LName = IP.LName AND ID.FName = IP.FName
	WHERE IP.Id	IS NULL;
	-- (61923 rows affected)
GO

-----------------------
-- Copy the Person table, don't insert duplicates
-----------------------
INSERT INTO odisee.dbo.Person(person_id, firstname, lastname)
	SELECT MIN(P.person_id), P.firstname, P.lastname
	FROM odisee.dbo.Person_tmp AS P
	GROUP BY P.firstname, P.lastname;
	-- (879492 rows affected)
GO

DROP TABLE odisee.dbo.Person_tmp;
GO

-----------------------
-- Movie
-----------------------
INSERT INTO odisee.dbo.Movie( movie_id, title, publication_year, price)
SELECT  CAST (IM.Id as int) AS movie_id, 
		CAST(IM.Name AS varchar(255)) AS title,
		CAST(IM.Year AS int) AS publication_year,
		0.00
	FROM MYIMDB.dbo.Imported_Movie AS IM;
	-- (388264 rows affected)
GO

-----------------------
-- Genre
-----------------------
INSERT INTO odisee.dbo.Genre( genre_name)
	SELECT  CAST(IG.Genre AS varchar(255)) AS genre_name
	FROM MYIMDB.dbo.Imported_Genre AS IG
	GROUP BY IG.Genre
	ORDER BY IG.Genre;
	-- (21 rows affected)
GO

-----------------------
-- Movie_Genre
-----------------------
INSERT INTO odisee.dbo.Movie_Genre(movie_id, genre_name)
	SELECT 
	CAST (IG.Id as int) AS movie_id,
	CAST(IG.Genre AS varchar(255)) AS genre_name
	FROM MYIMDB.dbo.Imported_Genre AS IG
	GROUP BY IG.Id, IG.Genre;
	-- (395114 rows affected)
GO

-----------------------
-- Movie_Directors
-----------------------
INSERT INTO odisee.dbo.Movie_Directors(movie_id, person_id)
SELECT DISTINCT
	CAST (IMD.Mid as int) AS movie_id,
	( SELECT P.person_id FROM odisee.dbo.Person AS P 
		WHERE 
	P.firstname COLLATE DATABASE_DEFAULT = ID.FName COLLATE DATABASE_DEFAULT 
	AND P.lastname COLLATE DATABASE_DEFAULT = ID.LName COLLATE DATABASE_DEFAULT 
	) as person_id
FROM MYIMDB.dbo.Imported_Movie_Directors AS IMD
INNER JOIN   MYIMDB.dbo.Imported_Directors AS ID ON IMD.Did = ID.Id
WHERE( SELECT P.person_id FROM odisee.dbo.Person AS P 
		WHERE 
	P.firstname COLLATE DATABASE_DEFAULT = ID.FName COLLATE DATABASE_DEFAULT 
	AND P.lastname COLLATE DATABASE_DEFAULT = ID.LName COLLATE DATABASE_DEFAULT 
	) IS NOT NULL

	--(198023 rows affected)
	GO

-----------------------
-- Movie_Cast
-----------------------

SELECT 
	CAST (IC.Mid as int) AS movie_id,
	CAST(IC.Pid AS int) AS person_id,
	left (right (IC.Role, len (IC.Role)-1), len (IC.Role)-2) AS role
	FROM MYIMDB.dbo.Imported_Cast AS IC