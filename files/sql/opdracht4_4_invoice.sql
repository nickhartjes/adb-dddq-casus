USE odisee1;

DECLARE
    @email_address varchar(255);
DECLARE
    @month int;
DECLARE
    @year int;
SET @email_address = 'nickhartjes@gmail.com';
SET @month = 4;
SET @year = 2019;


-----------------------
-- Invoice User
-----------------------
SELECT U.email_address,
       U.first_name,
       U.last_name,
       DateName(month, DateAdd(month, @month, -1)) + ' ' + CAST(@year AS VARCHAR(4)) AS month,
       (
           SELECT TOP 1 monthly_fee
           FROM UserSubscription US
           WHERE US.email_address = @email_address
             AND ((subscription_startdate <= datefromparts(@year, @month, 1)
               AND subscription_enddate >= datefromparts(@year, @month, 1)
                      )
               OR (subscription_startdate <= datefromparts(@year, @month, 1)
                   AND subscription_enddate IS NULL
                      ))
           ORDER BY subscription_startdate DESC
       ) AS monthly_fee,
       C.country_name,
       C.country_currency_name
FROM [User] AS U
INNER JOIN Country C on U.country_name = C.country_name
WHERE U.email_address = @email_address


-----------------------
-- Invoice entries
-----------------------
SELECT P.product_id,
       P.title,
       P.product_type,
       PU.purchase_date,
       CASE
           WHEN P.product_type = 'Game'
               THEN DGP.default_price
           ELSE P.movie_default_price
           END AS default_price,
       C.country_currency_name,
       CASE
           WHEN P.product_type = 'Game'
               THEN DGP.default_price - PU.price
           ELSE P.movie_default_price - PU.price
           END AS discount,
       PU.price
FROM odisee1.dbo.Purchase AS PU
         INNER JOIN Product P on PU.product_id = P.product_id
         INNER JOIN [User] U on PU.email_address = U.email_address
         LEFT JOIN DefaultGamePrice DGP on P.product_id = DGP.product_id and U.country_name = DGP.country_name
         LEFT JOIN Country C on U.country_name = C.country_name
WHERE PU.email_address = @email_address
  AND MONTH(PU.purchase_date) = @month
  AND YEAR(PU.purchase_date) = @year;