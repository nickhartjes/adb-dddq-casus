-----------------------
-- Negative selection query
-----------------------
SELECT M.Id
FROM dbo.Imported_Movie AS M
GROUP BY M.Id
HAVING COUNT(*) > 1;
GO
-- 0 rows retrieved in 381 ms (execution: 334 ms, fetching: 47 ms)

-----------------------
-- Remove duplicates except one
-----------------------
WITH duplicates AS
         (
             SELECT Id, ROW_NUMBER() OVER (PARTITION BY Id ORDER BY Id) RowNumber
             FROM dbo.Imported_Movie
         )
DELETE
FROM duplicates
WHERE RowNumber > 1
GO
-- completed in 484 ms

-----------------------
-- Test set negative selection query
-----------------------
DECLARE
    @id varchar(10);
SET @id = 999999;

-- Check base
SELECT COUNT(*) AS Base
FROM dbo.Imported_Movie AS M
WHERE M.Id = @id;
-- Result = 0

-- Insert test set
INSERT INTO dbo.Imported_Movie (Id, Nyr, "Name", "Year", "Rank")
VALUES (@id, 'test', 'test', 'test', 'test'),
       (@id, 'test', 'test', 'test', 'test');

-- Check inserted data
SELECT COUNT(*) AS Inserted
FROM dbo.Imported_Movie AS M
WHERE M.Id = @id;
-- Result = 2

-- Now run the negative selection query

-- Remove test data
DELETE
FROM dbo.Imported_Movie
WHERE Id = @id;