-----------------------
-- Negative selection query
-----------------------
SELECT G.Id, G.Genre
FROM dbo.Imported_Genre AS G
GROUP BY G.Id, G.Genre
HAVING COUNT(*) > 1;
-- 21 rows retrieved starting from 1 in 914 ms (execution: 860 ms, fetching: 54 ms)

-----------------------
-- Remove duplicates except one
-----------------------
WITH duplicates AS
         (
             SELECT Id, Genre, ROW_NUMBER() OVER (PARTITION BY Id, Genre ORDER BY Id, Genre) RowNumber
             FROM dbo.Imported_Genre
         )
DELETE
FROM duplicates
WHERE RowNumber > 1
GO
