SELECT G.Id, Count(*)
FROM dbo.Imported_Genre AS G
       LEFT JOIN dbo.Imported_Movie AS M ON G.Id = M.Id
GROUP BY G.Id
HAVING COUNT(*) <= 0;