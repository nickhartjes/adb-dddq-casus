use MYIMDB
go

create table Imported_Cast_Clean
(
    Pid  varchar(10),
    Mid  varchar(10),
    Role varchar(4000)
)
go

create clustered index IX_MP_Clean
    on Imported_Cast_Clean (Pid, Mid)
go

create index IX_Imported_Cast_Clean
    on Imported_Cast_Clean (Mid)
go

create table Imported_Director_Genre_Clean
(
    Did   varchar(10),
    Genre varchar(4000),
    Prob  varchar(100)
)
go

create table Imported_Directors_Clean
(
    Id    varchar(10),
    FName varchar(4000),
    LName varchar(4000)
)
go

create table Imported_Genre_Clean
(
    Id    varchar(10),
    Genre varchar(4000)
)
go

create index IX_Imported_Genre_Clean
    on Imported_Genre_Clean (Id)
go

create table Imported_Movie_Clean
(
    Id   varchar(10),
    Nyr  varchar(4000),
    Name varchar(4000),
    Year varchar(4000),
    Rank varchar(4000)
)
go

create index IX_Imported_Movie_Clean
    on Imported_Movie_Clean (Id)
go

create table Imported_Movie_Directors_Clean
(
    Did varchar(10),
    Mid varchar(10)
)
go

create table Imported_Person_Clean
(
    Id     varchar(10),
    FName  varchar(4000),
    LName  varchar(4000),
    Gender varchar(4000)
)
go