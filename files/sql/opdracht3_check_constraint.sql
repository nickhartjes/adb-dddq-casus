-- Constraints opdracht 2:
-- Purchase:
-- Purchase_Date mag niet in de toekomst liggen.
-- Price mag niet negatief zijn.

-- Movie:
-- Duration kan niet negatief zijn.
ALTER TABLE odisee1.dbo.Movie
    ADD CONSTRAINT CK_Movie_duration_not_negative
        CHECK (duration >= 0);
-- Default_price mag niet negatief zijn.

-- Reviewdate:
-- Review_date mag niet in de toekomst liggen.
ALTER TABLE odisee1.dbo.Review
    ADD CONSTRAINT CK_Review_date
        CHECK (review_date <= GETDATE());

-- Category:
-- Score is een waarde tussen 1 en 10.
ALTER TABLE odisee1.dbo.Category
    ADD CONSTRAINT CK_Category_Score_limits
        CHECK (score >= 1 AND score <= 10);

-- User_Subscription:
-- Monthly_fee mag niet negatief zijn.
ALTER TABLE odisee1.dbo.User_Subscription
    ADD CONSTRAINT CK_User_Subscription_monthlyfee_not_negative
        CHECK (monthly_fee >= 0);

-- Subscription:
-- Discount_percentage is een waarde tussen 0 en 100.
ALTER TABLE odisee1.dbo.Subscription
    ADD CONSTRAINT CK_Discount_limits
        CHECK (discount_percentage >= 0 AND discount_percentage <= 100);

-- Monthly_fee mag niet negatief zijn.
ALTER TABLE odisee1.dbo.Subscription
    ADD CONSTRAINT CK_Subscription_monthlyfee_not_negative
        CHECK (monthly_fee >= 0);

-- Invoice:
-- Subtotal mag niet negatief zijn.
-- Total_price mag niet negatief zijn.
ALTER TABLE odisee1.dbo.Invoice
    ADD CONSTRAINT CK_Invoice_totalprice_not_negative
        CHECK (total_price >= 0);

-- User:
-- E-mailaddress moet een @ bevatten.

-- Gender moet M of F zijn.
ALTER TABLE odisee1.dbo.User
    ADD CONSTRAINT CK_User_Gender
        CHECK (gender in ('M', 'F'));
-- Birthdate moet in het verleden liggen.

-- Person:
-- Gender moet M of F zijn.
ALTER TABLE odisee1.dbo.Person
    ADD CONSTRAINT CK_Person_Gender
    CHECK (gender in ('M', 'F'));
-- Person_type kan alleen maar Cast_member of Director zijn.

-- Review:
-- Average_score moet tussen 1 en 10 liggen.
ALTER TABLE odisee1.dbo.Review
    ADD CONSTRAINT CK_Average_Score_limits
        CHECK (average_score >= 1 AND average_score <= 10);

-- Constraints opdracht 3:
-- Game:
-- Number_of_online_players mag niet negatief zijn.
ALTER TABLE odisee1.dbo.Product
    ADD CONSTRAINT CK_OnlinePlayers_price_negative
        CHECK (number_of_online_players > 0);

-- Default_Game_Price:
-- Default_price moet positief zijn.
ALTER TABLE odisee1.dbo.Product
    ADD CONSTRAINT CK_Product_price_negative
        CHECK (average_score >= 1 AND average_score <= 10);

