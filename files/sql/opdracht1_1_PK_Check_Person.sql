-----------------------
-- Negative selection query
-----------------------
SELECT P.Id
FROM dbo.Imported_Person AS P
GROUP BY P.Id
HAVING COUNT(*) > 1;
-- 0 rows retrieved in 1 s 428 ms (execution: 1 s 406 ms, fetching: 22 ms)
GO

-----------------------
-- Remove duplicates except one
-----------------------
WITH duplicates AS
         (
             SELECT Id, ROW_NUMBER() OVER (PARTITION BY Id ORDER BY Id) RowNumber
             FROM dbo.Imported_Person
         )
DELETE
FROM duplicates
WHERE RowNumber > 1
GO
-- completed in 4 s 117 ms

-----------------------
-- Test set negative selection query
-----------------------
DECLARE
    @id varchar(10);
SET @id = 999999;

-- Check base
SELECT COUNT(*) AS Base
FROM dbo.Imported_Person AS P
WHERE P.Id = @id;
-- Result = 0

-- Insert test set
INSERT INTO dbo.Imported_Person(Id, FName, LName, Gender)
VALUES (@id, 'test', 'test', 'test'),
       (@id, 'test', 'test', 'test');

-- Check inserted data
SELECT COUNT(*) AS Base
FROM dbo.Imported_Person AS P
WHERE P.Id = @id;
-- Result = 2

-- Now run the negative selection query

-- Remove test data
DELETE
FROM dbo.Imported_Person
WHERE Id = @id;