-----------------------
-- Negative selection query
-----------------------
SELECT DG.*
FROM dbo.Imported_Director_Genre AS DG
       LEFT JOIN
     dbo.Imported_Genre AS G
     ON DG.Genre = G.Genre
WHERE G.Genre IS NULL
-- Time 0:06 98940 Records

-----------------------
-- Delete
-----------------------
DELETE DG
FROM dbo.Imported_Director_Genre AS DG
         LEFT JOIN
     dbo.Imported_Genre AS G
     ON DG.Genre = G.Genre
WHERE G.Genre IS NULL
