-----------------------
-- Negative selection query
-----------------------
SELECT MD.*
FROM dbo.Imported_Movie_Directors AS MD
WHERE MD.Did NOT IN (
    SELECT P.Id
    FROM dbo.Imported_Person AS P
);
-- Time 1:47 10601 Records

SELECT MD.*
FROM dbo.Imported_Movie_Directors AS MD
         LEFT JOIN
     dbo.Imported_Person AS P
     ON MD.Did = P.Id
WHERE P.Id IS NULL
-- Time 0:01 10601 Records

-----------------------
-- Delete
-----------------------
DELETE MD
FROM dbo.Imported_Movie_Directors AS MD
         LEFT JOIN
     dbo.Imported_Person AS P
     ON MD.Did = P.Id
WHERE P.Id IS NULL
-- 10601 rows affected in 3 s 994 ms

-----------------------
-- Test set
-----------------------
DECLARE
    @mid varchar(10);
DECLARE
    @did varchar(10);
SET @mid = 1;
SET @did = 1;

-- Check base
SELECT COUNT(*)
FROM dbo.Imported_Movie_Directors AS MD
WHERE MD.Did = @did
  AND MD.Mid = @mid;
-- Result = 0

SELECT COUNT(*)
FROM dbo.Imported_Person AS P
WHERE P.Id = @did;
-- Result = 0

-- Insert test set
INSERT INTO dbo.Imported_Movie_Directors (Did, Mid)
VALUES (@did, @mid);

-- Check inserted data
SELECT COUNT(*)
FROM dbo.Imported_Movie_Directors AS MD
WHERE MD.Did = @did
  AND MD.Mid = @mid;
-- Result = 1

-- Now run the negative selection query
-- the count needs to be 10602

-- Remove test data
DELETE
FROM dbo.Imported_Movie_Directors
WHERE Mid = @mid
  AND Did = @did;