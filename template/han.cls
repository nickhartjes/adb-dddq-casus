
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{template/han}

\LoadClass{article}
\RequirePackage[margin=3cm,a4paper]{geometry}

% Packages
\RequirePackage[utf8]{inputenc}
\RequirePackage{listings}               % For showing code
\RequirePackage{color}                  % For color
\RequirePackage{graphicx}               % For images
\RequirePackage[sfdefault]{cabin}       % For font
\RequirePackage[T1]{fontenc}            % For font
\RequirePackage{courier}                % For code font
\RequirePackage{wallpaper}              % For background
\RequirePackage[many]{tcolorbox}        % For whitebox behind title
\RequirePackage[dutch]{babel}           % Nederlands taalpakket

% Bibliograpy
\RequirePackage[style=apa]{biblatex}
\addbibresource{publication.bib}

%Colors
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.94, 0.97, 1.0}

%Background image for the first page
\ThisCenterWallPaper{1.6}{template/images/background.eps} % Add wallpaper

% Transparent backgourd color box
\newtcolorbox{backgroundTransparent}[1][]{
    width=\textwidth,
    arc=3mm,
    %    auto outer arc,
    boxsep=0cm,
    toprule=2pt,
    leftrule=2pt,
    bottomrule=2pt,
    rightrule=2pt,
    colframe=white,
    fontupper=\raggedleft\fontsize{16pt}{16pt}\itshape,
    breakable,
    nobeforeafter,
    enhanced jigsaw,
    opacityframe=0.5,
    opacityback=0.7
}


% Style for code
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\footnotesize\ttfamily,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    %numbers=left,
    numbers=none,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}
\lstset{style=mystyle}

% MakeTitle
\renewcommand{\maketitle}{
    \begin{titlepage}

        \begin{minipage}[t]{1.0\textwidth}
            \includegraphics[scale=0.07]{template/images/han/han-hogeschool-van-arnhem-en-nijmegen-logo.eps}
        \end{minipage}
        \begin{minipage}[t]{1.0\textwidth}
            \vspace{-1.7cm}
            \hfill
            \includegraphics[scale=0.6]{template/images/han/logo_ica.eps}
        \end{minipage}

        \begin{minipage}{\textwidth}

            \vspace{1.5cm}

            \begin{minipage}[t]{1.0\textwidth}
                \begin{backgroundTransparent}[]
                    \begin{flushright}
                        \Huge{\@title}
                    \end{flushright}
                    \begin{flushright}
                        \Large{\today}
                    \end{flushright}
                \end{backgroundTransparent}
            \end{minipage}
        \end{minipage}

        \mbox{} % Allign to the bottom
        \vfill

        \small
        \begin{flushleft}
            \textbf{Docent(en):} \newline
            \docenten
        \end{flushleft}

        \begin{flushleft}
            \textbf{Student(en):} \newline
            \@author
        \end{flushleft}

        \begin{flushleft}
            Vak: \vak
        \end{flushleft}

    \end{titlepage}
}

\lstset{literate=
  {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
  {Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
  {à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
  {À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
  {ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
  {Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
  {â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
  {Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
  {œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
  {ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
  {ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
  {€}{{\euro}}1 {£}{{\pounds}}1 {«}{{\guillemotleft}}1
  {»}{{\guillemotright}}1 {ñ}{{\~n}}1 {Ñ}{{\~N}}1 {¿}{{?`}}1
}
